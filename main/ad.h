/*
 * ad.h
 *
 *  Created on: Oct 18, 2022
 *      Author: zamek
 */

#ifndef MAIN_AD_H_
#define MAIN_AD_H_

#include <stdint.h>
#include "freertos/FreeRTOS.h"

//!!! Define the channel number
#define MAX_CHANNEL

#define AD_MAX (4096)
#define AD_MIN (0)

BaseType_t ad_init();

//!!! pass channel index to function & use it
BaseType_t ad_get(uint16_t *value, TickType_t ticks);

//!!! pass channel index to function & use it
BaseType_t ad_get_temperature(float *value, TickType_t ticks);

BaseType_t ad_deinit();




#endif /* MAIN_AD_H_ */
